docker kill binocular
docker rm binocular
docker build -t binocular .
docker create --name binocular -v /Users/mxml/Desktop/:/data/ -v /var/run/docker.sock:/var/run/docker.sock -p 8080:8080 binocular
docker start binocular