package de.mxdl.binocular.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Static functions that are being used by multiple classes
 */
public class StaticFunctions {
    public static String buildJsonFromList(List list, String jsonArrayName) {
        StringBuilder jobJson = new StringBuilder();
        jobJson.append("{\"" + jsonArrayName + "\":[");

        String delimiter = "";
        for (Object obj : list) {
            jobJson.append(delimiter);
            delimiter = ",";
            String objJson = "";
            try {
                objJson = new ObjectMapper().writeValueAsString(obj);
            } catch (JsonProcessingException e) {
                objJson = obj.toString();
            }

            jobJson.append(objJson);

        }
        jobJson.append("]}");
        return jobJson.toString();
    }

    public static void writeStringToFile(String input, String fileDest) throws IOException {
        File file = new File(fileDest);
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(input);
        fileWriter.flush();
        fileWriter.close();
    }

    public static String objectToJson(Object obj) {
        String body = "{}";
        try {
            body = new ObjectMapper().writeValueAsString(obj);
        } catch (JsonProcessingException e) {
        }
        return body;

    }
}
