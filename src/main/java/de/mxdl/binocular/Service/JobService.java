package de.mxdl.binocular.Service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.Container;
import com.spotify.docker.client.messages.ContainerConfig;
import de.mxdl.binocular.BinocularApplication;
import de.mxdl.binocular.Data.Entity.HealthCommand;
import de.mxdl.binocular.Data.Entity.Job;
import de.mxdl.binocular.Data.Entity.RepairStep;
import de.mxdl.binocular.Data.Repository.JobRepository;
import org.apache.log4j.Logger;
import org.hibernate.id.IdentifierGenerationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Service that offers all needed logical functionality for the job endpoint
 */
@Service
public class JobService {

    private static Logger log = Logger.getLogger(BinocularApplication.class.getName());

    @Autowired
    ContainerService containerService;

    @Autowired
    JobRepository jobRepository;

    public ResponseEntity getJobs() {
        List<Job> jobs = jobRepository.findAll();
        String json = StaticFunctions.buildJsonFromList(jobs, "jobs");
        return ResponseEntity.ok(json);
    }

    @Transactional
    public ResponseEntity getJobById(int id) {
        Job job = jobRepository.findById(id);
        if (job == null) {
            return ResponseEntity.status(404).build();
        }
        return ResponseEntity.ok(job.toString());

    }

    /***
     * Creates a job if there is not already a job for the container with the name and there is a container with the given name
     * @param body the json body describing a job
     * @return A response according to the success or error
     */

    @Transactional
    public ResponseEntity createJobCheckName(String body) {
        String name;
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jobJson = mapper.readTree(body);

            name = jobJson.get("containerName").asText();
            if (!containerService.containerExistsByName(name)) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            }
        } catch (IOException e) {
            log.error("IO Error " + e.getCause() + " " + e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } catch (DockerException | InterruptedException serverException) {
            log.error("DockerException: " + serverException.getCause() + " " + serverException.getMessage());
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
        if (name == null || name.equals("") || jobRepository.findByContainerNameIgnoreCase(name) != null) {
            return ResponseEntity.status(HttpStatus.valueOf(409)).build();
        }


        return createJob(body);
    }

    /**
     * Creates a job based on the given body
     *
     * @param body the body which describes a job object
     * @return a responseentity which holds a statuscode based on the success
     */
    @Transactional
    public ResponseEntity createJob(String body) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        Job job;

        try {
            body = removeEmptyArrayObjects(body);
            job = objectMapper.readValue(body, Job.class);
            job = removeNullObjectsFromJob(job);
            job = createJobContainer(job, true);
            if (job != null) {
                jobRepository.save(job);
                log.info("Created job: " + job.getId());
                return ResponseEntity.ok(job.toString());
            }

        } catch (IOException | IdentifierGenerationException e) {
            log.error("Error creating job: " + e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        return ResponseEntity.status(500).build();
    }

    @Transactional
    public ResponseEntity deleteJob(int id) {
        Job toBeDeleted = jobRepository.findById(id);
        if (toBeDeleted == null) {
            return ResponseEntity.status(404).build();
        }
        jobRepository.delete(toBeDeleted);
        return ResponseEntity.ok().build();
    }

    /**
     * updates a job and its container
     *
     * @param id   the id of the job
     * @param body the body which the describes the updated job object
     * @return a responseentity which holds a statuscode based on the success
     */
    @Transactional
    public ResponseEntity patchJob(int id, String body) {
        ObjectMapper objectMapper = new ObjectMapper();

        Job job = jobRepository.findById(id);
        Job backupJob = job;
        try {
            String correctJson = removeEmptyArrayObjects(body);


            Job tempJob = objectMapper.readValue(correctJson, Job.class);
            tempJob = removeNullObjectsFromJob(tempJob);
            if (!tempJob.toString().equals(job.toString())) {
                job.update(tempJob);
                job.setCreatingContainer(true);
                job.resetRepairStepExecution();
                jobRepository.save(job);

                job = createJobContainer(job, false);
            }
            if (job != null) {
                job.setUnderRepair(false);
                job.setUnrepairable(false);
                job.setCreatingContainer(false);
                jobRepository.save(job);
                log.info("Updated job: " + job.toString());
                return ResponseEntity.ok(job.toString());
            }
        } catch (IOException io) {
            log.error("IO Error patching job: " + io.getMessage());
            if (backupJob != null) jobRepository.save(backupJob);
            return ResponseEntity.status(500).body(io.getMessage());
        } catch (IdentifierGenerationException ident) {
            log.error("Ident Error patching job: " + ident.getMessage());
            if (backupJob != null) jobRepository.save(backupJob);
            return ResponseEntity.status(400).body(ident.getMessage());
        }


        job.setCreatingContainer(false);
        jobRepository.save(job);
        return ResponseEntity.status(500).build();
    }

    /**
     * Updates the container of a job with the parameters of the job
     *
     * @param job    the job
     * @param newJob whether or not the job was just added
     * @return the job with updated information
     */
    @Transactional
    Job createJobContainer(Job job, boolean newJob) {
        String containerName = job.getContainerName();

        if (containerName != null) {
            try {

                String containerId = containerService.getContainerId(containerName);
                Container container = containerService.getContainerById(containerId);
                ContainerConfig config = containerService.containerToConfig(container);

                config = buildHealthCheck(job, config, newJob);

                ResponseEntity updateResponse = containerService.updateContainer(containerId, config);
                if (updateResponse.getStatusCodeValue() != 200) {
                    log.error("Container update failed: " + updateResponse.getBody());
                    return null;
                }
            } catch (DockerException | InterruptedException | NullPointerException e) {
                log.error("Error creating job container :" + e.getMessage());
                return null;
            }

        }
        return job;
    }

    /**
     * Removes empty objects from a json string
     *
     * @param body the string from which the empty object should be removed
     * @return the string with the empty objects removed
     */
    private String removeEmptyArrayObjects(String body) {
        return body.replaceAll("(\\{\\})(,?)", "");
    }

    private Job removeNullObjectsFromJob(Job job) {
        List<RepairStep> repairSteps = job.getRepairSteps();
        List<RepairStep> toRemove = new ArrayList<>();

        for (RepairStep step : repairSteps) {
            if (step.getType() == null) {
                toRemove.add(step);
            }
        }
        repairSteps.removeAll(toRemove);
        job.setRepairSteps(repairSteps);

        List<HealthCommand> healthCommands = job.getHealthCommands();
        List<HealthCommand> toRemoveHealth = new ArrayList<>();
        for (HealthCommand command : healthCommands) {
            if (command.getType() == null) {
                toRemoveHealth.add(command);
            }
        }
        healthCommands.removeAll(toRemoveHealth);
        job.setHealthCommands(healthCommands);
        return job;
    }


    private ContainerConfig buildHealthCheck(Job job, ContainerConfig config, boolean newJob) {

        List<String> checkCommands = getJobChecks(job);

        //only add checks from container when this is a newly created job
        if (newJob) {
            List<String> containerCommands = getContainerChecks(config);


            for (String checkCommand : containerCommands) {
                if (!checkCommand.toLowerCase().equals("cmd") && !checkCommand.toLowerCase().equals("cmd-shell")) {
                    job.addHealthCommand(new HealthCommand(checkCommand, job));
                    checkCommands.add(checkCommand);
                }
                if (checkCommand.toLowerCase().equals("cmd")) job.setCmd("CMD");
            }
            jobRepository.save(job);

        }
        if (checkCommands.size() > 0) {
            //build the new healthcheck list and attach it to the config
            ContainerConfig.Healthcheck.Builder builder = ContainerConfig.Healthcheck.builder()
                    .interval(job.getHealthInterval() * 1000000000)
                    .retries(job.getHealthRetries())
                    .timeout(job.getHealthTimeout() * 1000000000)
                    .startPeriod(job.getHealthStartPeriod() * 1000000000);

            builder.test(Arrays.asList(job.getCmd(), buildHealthString(checkCommands)));
            ContainerConfig.Healthcheck newCheck = builder.build();

            config = config.toBuilder().healthcheck(newCheck).build();
        }
        return config;

    }

    /**
     * Get all healthchecks of a config
     *
     * @param config the config
     * @return a list of all healthchecks
     */
    private List<String> getContainerChecks(ContainerConfig config) {
        //add existing health commands and add them to the job
        ContainerConfig.Healthcheck check = config.healthcheck();
        if (check != null) {
            List<String> commands = check.test();
            if (commands != null) {
                return commands;
            }
        }
        return new ArrayList<>();
    }


    private List<String> getJobChecks(Job job) {
        List<String> checkCommands = new ArrayList<>();
        for (HealthCommand command : job.getHealthCommands()) {
            HealthCommand.CHECKTYPE type = command.getType();

            if (type == HealthCommand.CHECKTYPE.Command) {
                checkCommands.add(command.getCommand());
            }
            if (type == HealthCommand.CHECKTYPE.HttpCall) {
                checkCommands.add("(wget --server-response -T 1 -t 1 --spider " + command.getUrl() + " 2>&1 | grep " + command.getStatusCode()+")");
            }
        }
        return checkCommands;
    }

    /**
     * Builds a list of all healthchecks and converts it to a string that is being used as healthcheck
     *
     * @param checks the list of string to be converted
     * @return the string
     */
    private String buildHealthString(List<String> checks) {
        StringBuilder checkList = new StringBuilder();
        String and = "";
        checkList.append("(");
        for (String checkToAdd : checks) {
            //only add real commands with
            if (!checkToAdd.toLowerCase().equals("cmd") || !checkToAdd.toLowerCase().equals("cmd-shell")) {
                checkList.append(and);
                checkList.append(checkToAdd.split("\\|\\|")[0]);
                and = "&&";
            }
        }

        //return unhealthy if one fails
        String removedExit = checkList.toString().replaceAll("&&( )*?exit( )*?0", "");

        if (removedExit.startsWith("((")) {
            removedExit = removedExit.replaceFirst("[(]", "");
            removedExit += (" && exit 0 || exit 1");
        } else removedExit += (") && exit 0 || exit 1");

        return removedExit;
    }

}
