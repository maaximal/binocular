package de.mxdl.binocular.Service;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.exceptions.DockerCertificateException;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.Container;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ContainerState;
import com.spotify.docker.client.messages.Volume;
import de.mxdl.binocular.BinocularApplication;
import de.mxdl.binocular.Data.Entity.AnalyticData;
import de.mxdl.binocular.Data.Entity.Job;
import de.mxdl.binocular.Data.Entity.RepairStep;
import de.mxdl.binocular.Data.Repository.JobRepository;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

/**
 * The service that takes on the repair of broken jobs
 */
@Service
public class RepairService {


    private static Logger log = Logger.getLogger(BinocularApplication.class.getName());
    private DockerClient docker;
    private JobRepository jobRepository;
    private ContainerService containerService;
    @Value("${analyticdata.savedir}")
    private String savedir;

    @Autowired
    RepairService(JobRepository jobRepository, ContainerService containerService) throws DockerCertificateException {
        this.jobRepository = jobRepository;
        this.containerService = containerService;
        this.docker = DefaultDockerClient.fromEnv().build();
    }

    /**
     * Initializer for the repair of a job.
     * Only starts the repair if the container for the job was not user exited and is not already under repair.
     *
     * @param job the job which should be repaired
     */
    @Transactional
    public void repairJob(Job job) {
        job = jobRepository.findById(job.getId());
        if (job.isUnderRepair()) return;
        try {
            Container c = containerService.getContainerByName(job.getContainerName());
            if (docker.inspectContainer(c.id()).state().exitCode() == 137) return;
        } catch (DockerException | InterruptedException e) {
            log.error(e.getMessage());
        }
        job.addAnalyticData(cumulateData(job));
        job.setUnderRepair(true);
        jobRepository.saveAndFlush(job);
        startRepair(job);
    }

    /**
     * Starts the repair of a job respectively the next step of a repair if it has already been started
     *
     * @param job the job which should be repaired
     */
    @Transactional
    public void startRepair(Job job) {
        if (job.isInStep()) return;
        job.setInStep(true);
        jobRepository.saveAndFlush(job);
        List<RepairStep> repairSteps = job.getRepairSteps();
        if (repairSteps.size() == 0) {
            job = addStandardRepair(job);
            jobRepository.save(job);
            log.info("Added default repairsteps to job: " + job.getId());
        }
        boolean hasNewName = false;
        for (RepairStep repairStep : repairSteps) {
            if (repairStep != null && repairStep.getType() != null) {
                if (repairStep.isExecuted() && repairStep.isReplaceOld()) hasNewName = true;
                if (!repairStep.isExecuted()) {
                    if (repairStep.getType() == RepairStep.REPAIRTYPE.Reset) {
                        log.warn("Running reset routine");
                        resetStep(repairStep, hasNewName);
                    } else if (repairStep.getType() == RepairStep.REPAIRTYPE.Replace) {
                        log.warn("Running replace routine");
                        replaceStep(repairStep, hasNewName);
                    } else if (repairStep.getType() == RepairStep.REPAIRTYPE.Restart) {
                        log.warn("Running restart routine");
                        restartStep(repairStep);
                    }

                    job.setInStep(false);
                    repairStep.setExecuted(true);
                    jobRepository.save(job);
                    return;
                }
            }
            //after all steps have been executed
            job.setUnderRepair(false);
            log.warn("No more steps to go trough: Unrepairable");
            job.setUnrepairable(true);
            resetRepairStepExecutionState(job);
        }
    }

    /**
     * Adds a set of standard repairsteps to a job
     *
     * @param job the job to which the repairsteps are added
     * @return the altered job
     */
    private Job addStandardRepair(Job job) {
        RepairStep repairStep = new RepairStep(RepairStep.REPAIRTYPE.Restart, false, job);
        job.addRepairStep(repairStep);
        repairStep = new RepairStep(RepairStep.REPAIRTYPE.Replace, false, job);
        job.addRepairStep(repairStep);
        repairStep = new RepairStep(RepairStep.REPAIRTYPE.Reset, false, job);
        job.addRepairStep(repairStep);
        return job;
    }

    /**
     * This step creates a new container based on the old one
     * It backs up all data from the container mounts and clears the mounts
     *
     * @param repairStep the step being executed
     * @param hasNewName whether or not the container has received a new name during the current repair
     */
    private void resetStep(RepairStep repairStep, boolean hasNewName) {
        Job job = repairStep.getJob();
        try {
            Container brokenContainer = containerService.getContainerByName(job.getContainerName());
            String broken = job.getContainerName();

            ContainerConfig containerConfig = containerService.containerToConfig(brokenContainer);
            containerService.killContainer(brokenContainer.id());
            String repairContainerName = job.getContainerName();

            if (containerConfig.hostConfig().binds() != null) {
                for (String bind : containerConfig.hostConfig().binds()) {
                    String dir = bind.split(":")[0];
                    if (!backupData(job, dir)) {
                        log.error("Data was not deleted since it could not be backed up.");
                        return;
                    }
                    emptyDirectory(dir);

                }
            }
            if (!repairStep.isReplaceOld() && !hasNewName) {
                job.getRepairedContainers().add(job.getContainerName());
                if (job.getRepairedContainers().size() == 0) {
                    repairContainerName = job.getContainerName() + 0;
                } else {
                    repairContainerName = job.getRepairedContainers().get(0) + job.getRepairedContainers().size();
                }
                job.setContainerName(repairContainerName);

            } else {
                containerService.deleteContainer(brokenContainer.id());
            }

            if (containerConfig.volumes() != null) {
                for (String volume : containerConfig.volumes()) {
                    Volume v = docker.inspectVolume(volume);
                    docker.removeVolume(volume);
                    docker.createVolume(v);

                }
            }
            containerService.addContainer(repairContainerName, containerConfig);
            jobRepository.save(job);
            log.info("Reset container with name: " + broken + " with container with name: " + repairContainerName);
        } catch (DockerException | InterruptedException | IOException e) {
            log.error("Error reseting: " + e.getMessage());
        }
    }

    /**
     * This step creates a new container based on the old one but does not alter any data
     *
     * @param repairStep the step being executed
     * @param hasNewName whether or not the container has received a new name during the current repair
     */
    //creates a new container with same everything and no data removal
    private void replaceStep(RepairStep repairStep, boolean hasNewName) {
        Job job = repairStep.getJob();
        try {
            Container brokenContainer = containerService.getContainerByName(job.getContainerName());
            String broken = job.getContainerName();

            ContainerConfig containerConfig = containerService.containerToConfig(brokenContainer);
            containerService.killContainer(brokenContainer.id());
            String repairContainerName = job.getContainerName();
            if (!repairStep.isReplaceOld() && !hasNewName) {
                job.getRepairedContainers().add(job.getContainerName());
                if (job.getRepairedContainers().size() == 0) {
                    repairContainerName = job.getContainerName() + 0;
                } else {
                    repairContainerName = job.getRepairedContainers().get(0) + job.getRepairedContainers().size();
                }
                job.setContainerName(repairContainerName);

            } else {
                containerService.deleteContainer(brokenContainer.id());
            }

            containerService.addContainer(repairContainerName, containerConfig);
            log.info("Replaced container with name: " + broken + " with container with name: " + repairContainerName);
        } catch (DockerException | InterruptedException e) {

            log.error("Error replacing: " + e.getMessage());
        }

    }


    /**
     * This step restarts the current container of the job
     *
     * @param repairStep the step being executed
     */
    private void restartStep(RepairStep repairStep) {
        Job job = repairStep.getJob();
        try {
            String id = containerService.getContainerId(job.getContainerName());
            if (id != null) {
                containerService.restartContainer(id);
                log.info("Restarted container " + id);
            }

        } catch (DockerException | InterruptedException e) {
            log.error("Error restarting: " + e.getMessage());
        }
    }


    /**
     * This function is the callback for the healthcheckservice and is being called when the healthstatus of a container under repair is queried
     * Based on the health state the job is either marked as repaired or is sent to be processed by the next repair step
     *
     * @param job         the job which is being monitored
     * @param healthState the current state of the container for the job
     */
    public void healthChange(Job job, ContainerState.Health healthState) {
        log.info("Update for job: " + job.toString());
        job = jobRepository.findById(job.getId());
        String status = healthState.status();
        if (job.isInStep()) return;
        if (status.equals("healthy")) {
            log.warn("Job was repaired successfully.");
            job.setUnderRepair(false);
            job.setUnrepairable(false);
            job.setInStep(false);
            getCurrentAnalyticData(job).setSuccessful(true);
            resetRepairStepExecutionState(job);
        } else if (status.equals("unhealthy")) {
            if (!job.isUnrepairable()) startRepair(job);
        }
        //else do nothing and wait for changes
    }

    /**
     * This function cumulates the logs for a job and saves them to the datasystem based on the job id and the timestamp of the incident.
     *
     * @param job the job for which the data should be cumulated
     * @return an AnalyticData Object which holds the timestamp
     */
    private AnalyticData cumulateData(Job job) {
        job = jobRepository.findById(job.getId());
        AnalyticData analyticData = new AnalyticData(job);
        analyticData.setTimestamp(new Timestamp(System.currentTimeMillis()));
        String analyticDataDir = savedir + "/" + job.getId() + "/" + analyticData.getTimestamp();
        try {
            boolean created = (new File(analyticDataDir)).mkdirs();
            if (!created) {
                log.error("Could not create directory for analytic data. Please check for wrong permissions.");
                return analyticData;
            }
            Container container = containerService.getContainerByName(job.getContainerName());
            String logs = containerService.getLogsForId(container.id());
            StaticFunctions.writeStringToFile(logs, analyticDataDir + "/logs.txt");
            String healthLogs = containerService.getHealthLogs(container.id());
            StaticFunctions.writeStringToFile(healthLogs, analyticDataDir + "/healthLogs.txt");

        } catch (DockerException | InterruptedException | IOException e) {
            log.error("Error cumulating data: " + e.getMessage());
        }
        return analyticData;
    }

    /**
     * Backs up the data to the filesystem based on the analyticdata of a job
     *
     * @param job the job for which the data should be backed up
     * @param dir the directory from which the data should be saved
     * @return whether or not the backup was successful
     */
    private boolean backupData(Job job, String dir) {
        AnalyticData last = getCurrentAnalyticData(job);
        if (last != null) {
            String data = savedir + "/" + job.getId() + "/" + last.getTimestamp() + "/backup";
            boolean created = (new File(data + dir)).mkdirs();
            if (!created) {
                log.error("Could not create directory for backup data. Please check for wrong permissions.");
                return false;
            }
            try {
                FileUtils.copyDirectory(new File(dir), new File(data + dir), true);
            } catch (IOException e) {
                return false;
            }
            return true;
        }
        return false;
    }

    private void resetRepairStepExecutionState(Job job) {
        for (RepairStep repairStep : job.getRepairSteps()) {
            repairStep.setExecuted(false);
        }
        jobRepository.save(job);
    }

    private void emptyDirectory(String dir) throws IOException {
        File directory = new File(dir);
        FileUtils.deleteDirectory(directory);
    }

    private AnalyticData getCurrentAnalyticData(Job job) {
        List<AnalyticData> analyticData = job.getAnalyticData();
        return analyticData.get(analyticData.size() - 1);
    }
}
