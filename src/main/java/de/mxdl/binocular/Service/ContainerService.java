package de.mxdl.binocular.Service;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.LogStream;
import com.spotify.docker.client.ProgressHandler;
import com.spotify.docker.client.exceptions.DockerCertificateException;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.exceptions.ImageNotFoundException;
import com.spotify.docker.client.messages.*;
import de.mxdl.binocular.BinocularApplication;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Service that offers all needed logical functionality for the container endpoint
 */
@Service
public class ContainerService {

    private static Logger log = Logger.getLogger(BinocularApplication.class.getName());

    private DockerClient docker;

    @Autowired
    ContainerService() throws DockerCertificateException {
        docker = DefaultDockerClient.fromEnv().build();
    }

    public Container getContainerById(String id) throws DockerException, InterruptedException {
        List<Container> containers = docker.listContainers(DockerClient.ListContainersParam.allContainers());
        for (Container container : containers) {
            if (container != null) {
                if (container.id().equals(id)) return container;
            }
        }
        return null;

    }

    protected void killContainer(String id) throws DockerException, InterruptedException {
        Container container = getContainerById(id);
        if (Objects.equals(container.state(), "running") || Objects.equals(container.state(), "starting")) {
            docker.killContainer(id);
        }

    }

    private void killContainer(Container container) throws DockerException, InterruptedException {
        if (!Objects.equals(container.state(), "exited")) {
            docker.killContainer(container.id());
        }
    }

    /**
     * Updates a running container with the given config
     * A temporary container will be created and started and only if this succeeds the original container will be updated
     *
     * @param id              the id of the container that should be updated
     * @param containerConfig the config based on which the container should be updated
     * @return a responseentity that holds a statuscode based on the success of the update
     */
    public ResponseEntity updateContainer(String id, ContainerConfig containerConfig) {
        try {
            if (containerExistsById(id)) {
                Container container = getContainerById(id);
                killContainer(container);
                String containerName = container.names().get(0).replace("/", "");
                String temporaryName = containerName + "_temporary";

                ResponseEntity response = addContainer(temporaryName, containerConfig);

                if (response.getStatusCodeValue() != 200) {
                    String tempContainerId = getContainerId(temporaryName);
                    if (containerExistsByName(temporaryName)) {
                        docker.removeContainer(tempContainerId);
                    }
                    docker.startContainer(id);
                    log.warn("Updating the container " + containerName + " was not successful");
                    return response;
                }
                //check if temp container starts and is healthy
                else {

                    String tempContainerId = response.getBody().toString().split("=")[1].split(",")[0];
                    Container tempContainer = getContainerById(tempContainerId);

                    //check if container even starts
                    if (Objects.equals(tempContainer.state(), "exited")) {
                        docker.removeContainer(tempContainerId);
                        docker.startContainer(id);
                        log.warn("The temporary container for container " + containerName + " did not start. Rolling back");
                        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("Container could not start with new parameters. Rolling back");
                    }

                    deleteContainer(tempContainerId);
                    deleteContainer(id);
                    addContainer(containerName, containerConfig);
                    log.info("The update of container " + containerName + " was successful");
                }

            }
        } catch (DockerException | InterruptedException e) {
            log.error("Error updating container: " + e.getMessage());
        }

        log.info("The update was successful");
        return ResponseEntity.ok().build();
    }

    /**
     * Generates a ContainerConfig from a running container
     *
     * @param container the container from which the config should be generated
     * @return the generated config
     * @throws DockerException
     * @throws InterruptedException
     */
    protected ContainerConfig containerToConfig(Container container) throws DockerException, InterruptedException {
        ContainerConfig config = docker.inspectContainer(container.id()).config();
        HostConfig.Builder hostConfigBuilder;
        if (config.hostConfig() != null) hostConfigBuilder = config.hostConfig().toBuilder();
        else hostConfigBuilder = HostConfig.builder();

        final Map<String, List<PortBinding>> portBindings = new HashMap<>();
        if (container.ports() != null) {
            for (Container.PortMapping mapping : container.ports()) {
                List<PortBinding> hostPorts = new ArrayList<>();
                hostPorts.add(PortBinding.of(mapping.ip(), mapping.publicPort().toString()));
                portBindings.put(mapping.privatePort().toString() + "/" + mapping.type(), hostPorts);
            }
        }

        List<String> binds = new ArrayList<>();
        if (container.mounts() != null) {
            for (ContainerMount mount : container.mounts()) {
                HostConfig.Bind newBind = HostConfig.Bind.from(mount.source())
                        .to(mount.destination())
                        .readOnly(!mount.rw())
                        .build();
                binds.add(newBind.toString());

            }
            hostConfigBuilder.binds(binds);
        }

        HostConfig hostConfig = hostConfigBuilder.portBindings(portBindings).build();

        config = config.toBuilder().hostConfig(hostConfig).build();

        return config;
    }

    /**
     * Returns the container id based on the given name
     *
     * @param name the name of the container for which the id should be parsed
     * @return the id of the container
     * @throws DockerException
     * @throws InterruptedException
     */
    protected String getContainerId(String name) throws DockerException, InterruptedException {
        Container c = getContainerByName(name);
        if (c != null) return c.id();
        return null;
    }

    protected Container getContainerByName(String name) throws DockerException, InterruptedException {
        List<Container> containers = docker.listContainers(DockerClient.ListContainersParam.allContainers());
        for (Container c : containers) {
            if (c != null && c.names() != null && name != null) {
                if (c.names().contains("/" + name)) return c;
            }
        }
        return null;
    }

    /**
     * Adds a container
     *
     * @param name            the name that the container should later have
     * @param containerConfig the config with which the container should be deployed
     * @return a responseentity based on the success of the addition
     */
    public ResponseEntity addContainer(String name, ContainerConfig containerConfig) {
        try {
            if (containerExistsByName(name)) {
                log.error("Container with name: " + name + " exists");
                return ResponseEntity.status(HttpStatus.CONFLICT).build();
            }

            pullImage(containerConfig.image());

            String body = docker.createContainer(containerConfig, name).toString();

            //parse container id from response
            String id = body.split("=")[1].split(",")[0];
            docker.startContainer(id);
            return ResponseEntity.ok(body);
        } catch (ImageNotFoundException image) {
            log.error("Error: " + image.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No image with that name was found in the default repository");
        } catch (DockerException | InterruptedException addException) {
            log.error("Error adding: " + addException.getMessage());
            return ResponseEntity.status(HttpStatus.FAILED_DEPENDENCY).body("Could not start temporary container");
        }

    }

    /**
     * Deletes a container
     *
     * @param id the id of the container that should be deleted
     * @return a responseentity that holds a statuscode based on the success of the deletion
     */
    public ResponseEntity deleteContainer(String id) {
        try {
            if (!containerExistsById(id)) return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            Container container = getContainerById(id);
            killContainer(container);
            docker.removeContainer(id);
        } catch (DockerException | InterruptedException deleteException) {
            return ResponseEntity.status(HttpStatus.FAILED_DEPENDENCY).build();
        }
        return ResponseEntity.ok().build();
    }

    public String getLogsForId(String id) {
        String logs = "";

        try (LogStream stream = docker.logs(id, DockerClient.LogsParam.stdout(), DockerClient.LogsParam.stderr())) {
            logs = stream.readFully();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return logs;
    }

    /**
     * Returns the health log of a given container with id
     *
     * @param id the id of the container
     * @return a string including all the log data for the healthchecks of the container
     */
    public String getHealthLogs(String id) {
        String logs = "";
        try {
            logs = docker.inspectContainer(id).state().health().log().toString();
        } catch (DockerException | InterruptedException e) {
            log.error("Error getting health logs:" + e.getMessage());
        }
        return logs;
    }

    public ResponseEntity listRunningContainers() {
        try {
            List<Container> containerList = docker.listContainers();
            List<Container> listWithoutSelf = new ArrayList<>();
            for (Container c : containerList) {
                if (!c.id().startsWith(BinocularApplication.id)) listWithoutSelf.add(c);
            }

            return ResponseEntity.ok(listWithoutSelf);
        } catch (DockerException | InterruptedException listException) {
            return ResponseEntity.status(500).build();
        }
    }


    public ResponseEntity listRunningContainersAsJson() {
        ResponseEntity entity = listRunningContainers();
        if (entity.getStatusCode() == HttpStatus.OK) {
            List<Container> containers = (List<Container>) entity.getBody();
            String containerJson = StaticFunctions.buildJsonFromList(containers, "containers");
            return ResponseEntity.ok(containerJson);
        }
        //if not successful
        return entity;
    }


    /***
     * Pulls an image with a given name and tag, if no tag is present it will use the latest tag
     * @param imageName the image with tag that should be pulled delimited by ":"
     * @throws DockerException
     * @throws InterruptedException
     */
    private void pullImage(String imageName) throws DockerException, InterruptedException, ImageNotFoundException {
        if (!imageName.contains(":")) imageName += ":latest"; //add latest tag so only one image is fetched

        final ProgressHandler progressHandler = message -> {

        };
        docker.pull(imageName, progressHandler);
    }

    public void restartContainer(String id) throws DockerException, InterruptedException {
        killContainer(id);
        docker.startContainer(id);

    }

    /**
     * Returns the healthstate of the container with the given name if it is present
     *
     * @param name the name of the container that is being parsed
     * @return the healtstate of the container
     * @throws DockerException
     * @throws InterruptedException
     */
    public ContainerState.Health getHealthState(String name) throws DockerException, InterruptedException {
        if (containerExistsByName(name)) {
            String id = getContainerId(name);
            ContainerState.Health healthState = docker.inspectContainer(id).state().health();
            if (healthState != null) {
                return healthState;
            }
        }
        return null;
    }

    /***
     * Checks if a container with a given id is present
     * @param id the id of the container to check for
     * @return true if a container with the id is present, false if no container with the id was found
     * @throws DockerException
     * @throws InterruptedException
     */
    private boolean containerExistsById(String id) throws DockerException, InterruptedException {
        return getContainerById(id) != null;
    }

    /***
     * Checks if a container with a given name is present
     * @param name the name of the container to check for
     * @return true if a container with the id is present, false if no container with the id was found
     * @throws DockerException
     * @throws InterruptedException
     */
    protected boolean containerExistsByName(String name) throws DockerException, InterruptedException {
        return getContainerByName(name) != null;
    }
}

