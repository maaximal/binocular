package de.mxdl.binocular.Service;

import de.mxdl.binocular.BinocularApplication;
import de.mxdl.binocular.Data.Entity.AnalyticData;
import de.mxdl.binocular.Data.Entity.Job;
import de.mxdl.binocular.Data.Repository.AnalyticDataRepository;
import de.mxdl.binocular.Data.Repository.JobRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Service that offers all needed logical functionality for the analytics endpoint
 */
@Service
public class AnalyticsService {

    private static Logger log = Logger.getLogger(BinocularApplication.class.getName());
    @Autowired
    AnalyticDataRepository analyticDataRepository;
    @Autowired
    JobRepository jobRepository;
    @Value("${analyticdata.savedir}")
    private String savedir;

    @Transactional
    public ResponseEntity getAnalyticsForJob(int jobId) {
        Job job = jobRepository.findById(jobId);
        List<AnalyticData> dataForJob = analyticDataRepository.findAllByJob(job);
        if (job == null) {
            return ResponseEntity.status(404).build();
        }

        String json = StaticFunctions.buildJsonFromList(dataForJob, "analytics");
        return ResponseEntity.ok(json);
    }

    @Transactional
    public ResponseEntity getAnalyticData(int id) {
        AnalyticData data = analyticDataRepository.findById(id);
        List<String> fileURLs = new ArrayList<>();
        String dataURL = "/"+data.getJob().getId()+"/"+data.getTimestamp()+"/";
        File dir = new File(savedir+dataURL);

        if(dir.exists()){
            File[] fileList = dir.listFiles();

            for (File file: fileList) {
                if (file.isFile()) {
                    fileURLs.add(file.getPath().toLowerCase().replace(savedir.toLowerCase(), ""));
                }
            }
        }

        if (fileURLs != null) {

            return ResponseEntity.ok(StaticFunctions.buildJsonFromList(fileURLs, "files"));
        }
        return ResponseEntity.status(404).build();
    }

    @Transactional
    public ResponseEntity delete(int id) {
        analyticDataRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

}
