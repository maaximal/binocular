package de.mxdl.binocular.Service;

import com.spotify.docker.client.exceptions.DockerCertificateException;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.ContainerState;
import de.mxdl.binocular.BinocularApplication;
import de.mxdl.binocular.Data.Entity.Job;
import de.mxdl.binocular.Data.Repository.JobRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Service that periodically checks the containers of all jobs for their health
 */
@Service
public class HealthCheckService {

    private static Logger log = Logger.getLogger(BinocularApplication.class.getName());

    private RepairService repairService;

    private JobRepository jobRepository;

    private ContainerService containerService;

    @Autowired
    HealthCheckService(RepairService repairService, JobRepository jobRepository, ContainerService containerService) throws DockerCertificateException {
        this.repairService = repairService;
        this.jobRepository = jobRepository;
        this.containerService = containerService;
    }

    /**
     * Starts a new check with the healthcheck class
     *
     * @param delay  the delay that should pass before the first check is started
     * @param period the time between the checks
     */
    public void startMonitoring(int delay, int period) {
        Timer timer = new Timer();
        timer.schedule(new HealthCheck(), delay, period);
    }

    private void unhealthy(Job job, ContainerState.Health healthStatus) {
        repairService.repairJob(job);
    }

    private void updateRepair(Job job, ContainerState.Health healthStatus) {
        repairService.healthChange(job, healthStatus);
    }

    private class HealthCheck extends TimerTask {

        /**
         * checks the health of all currently added jobs and reports it to the repairservice
         */
        @Transactional
        public void run() {
            try {
                for (Job job : jobRepository.findAll()) {

                    if (job.isCreatingContainer() || job.isInStep()) {
                        //do not check health, container is being modified
                        return;
                    } else if (job.isUnderRepair() || job.isUnrepairable()) {
                        ContainerState.Health healthState = containerService.getHealthState(job.getContainerName());
                        if (healthState != null) {
                            updateRepair(job, healthState);
                        }
                    } else if (!job.isUnderRepair() && !job.isUnrepairable()) {
                        ContainerState.Health healthState = containerService.getHealthState(job.getContainerName());
                        if (healthState != null) {
                            if (healthState.status().equals("unhealthy")) {
                                unhealthy(job, healthState);
                            }
                        }
                    }
                }

            } catch (DockerException | InterruptedException e) {
                log.error("Error checking health" + e.getMessage());
            }
        }
    }

}
