package de.mxdl.binocular;

import de.mxdl.binocular.Service.HealthCheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.net.InetAddress;
import java.net.UnknownHostException;

@SpringBootApplication
public class BinocularApplication {

    public static String id;
    @Autowired
    HealthCheckService healthCheckService;

    public static void main(String[] args) {
        SpringApplication.run(BinocularApplication.class, args);
    }

    @PostConstruct
    public void init() throws UnknownHostException {
        healthCheckService.startMonitoring(0, 5000);
        id = InetAddress.getLocalHost().getHostName();
    }

}
