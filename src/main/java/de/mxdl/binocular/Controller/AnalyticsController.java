package de.mxdl.binocular.Controller;

import de.mxdl.binocular.Service.AnalyticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


/**
 * Controller for the /api/analytics endpoint
 * Handles the listing and deletion of analytics
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/api/analytics")
public class AnalyticsController {
    @Autowired
    AnalyticsService analyticsService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity getAnalyticsByJobId(@PathVariable(value = "id") int id) {
        return analyticsService.getAnalyticsForJob(id);
    }

    @RequestMapping(value = "/{id}/{dataid}", method = RequestMethod.GET)
    public ResponseEntity getAnalyticsByDataId(@PathVariable(value = "dataid") int dataId) {
        return analyticsService.getAnalyticData(dataId);
    }

    @RequestMapping(value = "/{id}/{dataid}", method = RequestMethod.DELETE)
    public ResponseEntity deleteAnalyticsByDataId(@PathVariable(value = "dataid") int dataId) {
        return analyticsService.delete(dataId);
    }


}
