package de.mxdl.binocular.Controller;


import de.mxdl.binocular.Service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/**
 * Controller for the /api/job endpoint
 * Handles the creation, listing and deletion of jobs
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/job")
public class JobController {

    @Autowired
    JobService jobService;

    @RequestMapping(value = {"", "/"}, method = RequestMethod.GET)
    public ResponseEntity getJobs() {
        return jobService.getJobs();

    }

    @RequestMapping(value = {"", "/"}, method = RequestMethod.POST)
    public ResponseEntity createJob(@RequestBody String body) {
        return jobService.createJobCheckName(body);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity getJobById(@PathVariable(value = "id") int id) {
        return jobService.getJobById(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteJobById(@PathVariable(value = "id") int id) {
        return jobService.deleteJob(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    public ResponseEntity patchJobById(@PathVariable(value = "id") int id, @RequestBody String body) {
        return jobService.patchJob(id, body);
    }

}
