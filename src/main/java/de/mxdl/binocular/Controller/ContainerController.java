package de.mxdl.binocular.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.Container;
import com.spotify.docker.client.messages.ContainerConfig;
import de.mxdl.binocular.Service.ContainerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controller for the /api/container endpoint
 * Handles the creation, listing and deletion of containers
 * Not all Mappings are used currently in the application
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/container")
public class ContainerController {
    @Autowired
    ContainerService containerService;

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public ResponseEntity getContainer() {
        return containerService.listRunningContainersAsJson();
    }

    @RequestMapping(value = {"/", ""}, method = RequestMethod.POST)
    public ResponseEntity addContainer(@RequestBody String body) {
        return containerService.addContainer("mysql", ContainerConfig.builder().image("mysql").build());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity getContainerById(@PathVariable(value = "id") String id) throws DockerException, InterruptedException {
        Container container = containerService.getContainerById(id);

        if (container == null) return ResponseEntity.status(404).build();
        return ResponseEntity.ok(container.toString());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteContainerById(@PathVariable(value = "id") String id) {
        return containerService.deleteContainer(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    public ResponseEntity patchContainerById(@PathVariable(value = "id") String id, @RequestBody String body) throws DockerException, InterruptedException {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            ContainerConfig containerConfig = objectMapper.readValue(body, ContainerConfig.class);
            return containerService.updateContainer(id, containerConfig);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

}
