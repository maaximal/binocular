package de.mxdl.binocular.Data.Repository;

import de.mxdl.binocular.Data.Entity.Job;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository for the job entries in the database
 * This is used to execute commands on the database level
 */
public interface JobRepository extends JpaRepository<Job, String> {

    Job findByContainerNameIgnoreCase(String containerName);

    Job findById(int id);
}
