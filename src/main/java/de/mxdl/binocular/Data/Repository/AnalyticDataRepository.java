package de.mxdl.binocular.Data.Repository;

import de.mxdl.binocular.Data.Entity.AnalyticData;
import de.mxdl.binocular.Data.Entity.Job;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Repository for the analytic data entries in the database
 * This is used to execute commands on the database level
 */
public interface AnalyticDataRepository extends JpaRepository<AnalyticData, String> {

    List<AnalyticData> findAllByJob(Job job);

    AnalyticData findById(int id);

    void deleteById(int id);
}
