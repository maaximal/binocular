package de.mxdl.binocular.Data.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Entity that holds the data of a repair step for a job
 */
@Entity
@Cacheable(false)
@Table(name = "repair_step")
public class RepairStep implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @JoinColumn(name = "job")
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    private Job job;
    private REPAIRTYPE type;
    private boolean replaceOld = false;
    private boolean executed = false;

    public RepairStep() {
    }

    public RepairStep(REPAIRTYPE type, boolean replaceOld, Job job) {
        this.type = type;
        this.replaceOld = replaceOld;
        this.job = job;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public REPAIRTYPE getType() {
        return type;
    }

    public void setType(REPAIRTYPE type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isExecuted() {
        return executed;
    }

    public void setExecuted(boolean executed) {
        this.executed = executed;
    }

    public boolean isReplaceOld() {
        return replaceOld;
    }

    public void setReplaceOld(boolean replaceOld) {
        this.replaceOld = replaceOld;
    }

    public enum REPAIRTYPE {Restart, Replace, Reset}
}
