package de.mxdl.binocular.Data.Entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Target;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Entity of a job
 * Holds all necessary information needed to repair the job in case of a failed healthcheck
 */
@Entity
@Cacheable(false)
@Table(name = "job", indexes = @Index(columnList = "id"))
public class Job {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private int id;

    @Column(name = "container_name")
    private String containerName;


    @Column(name = "repaired_containers")
    @Target(value = ArrayList.class)
    private List<String> repairedContainers = new ArrayList<>();

    @Column(name = "health_interval")
    private Long healthInterval = 15L;

    @Column(name = "health_retries")
    private Integer healthRetries = 3;

    @Column(name = "health_timeout")
    private Long healthTimeout = 2L;

    @Column(name = "health_startperiod")
    private Long healthStartPeriod = 8L;

    @Column(name = "unrepairable")
    private boolean unrepairable = false;

    @Column(name = "under_repair")
    private boolean underRepair = false;

    @Column(name = "creating_container")
    private boolean creatingContainer = false;

    @Column(name = "cmd")
    private String cmd = "CMD-SHELL";

    @Column(name = "in_in_step")
    private boolean isInStep = false;

    @JsonManagedReference
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "job")
    private List<HealthCommand> healthCommands;

    @JsonManagedReference
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "job")
    private List<AnalyticData> analyticData;

    @JsonManagedReference
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "job")
    private List<RepairStep> repairSteps;

    public Job() {

    }

    public void update(Job job) {
        this.healthInterval = job.healthInterval;
        this.healthTimeout = job.healthTimeout;
        this.healthRetries = job.healthRetries;
        this.underRepair = false;
        this.unrepairable = false;
        this.isInStep = false;

        if (this.healthCommands != null) this.healthCommands.clear();
        if (job.healthCommands != null && job.healthCommands.size() > 0) this.healthCommands.addAll(job.healthCommands);

        if (this.repairSteps != null) this.repairSteps.clear();
        if (job.repairSteps != null && job.repairSteps.size() > 0) this.repairSteps.addAll(job.repairSteps);

        if (this.analyticData != null) this.analyticData.clear();
        if (job.analyticData != null && job.analyticData.size() > 0) this.analyticData.addAll(job.analyticData);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContainerName() {
        return containerName;
    }

    public void setContainerName(String containerName) {
        this.containerName = containerName;
    }

    public Long getHealthInterval() {
        return healthInterval;
    }

    public void setHealthInterval(Long healthInterval) {
        this.healthInterval = healthInterval;
    }

    public Integer getHealthRetries() {
        return healthRetries;
    }

    public void setHealthRetries(Integer healthRetries) {
        this.healthRetries = healthRetries;
    }

    public Long getHealthStartPeriod() {
        return healthStartPeriod;
    }

    public void setHealthStartPeriod(Long healthStartPeriod) {
        this.healthStartPeriod = healthStartPeriod;
    }

    public Long getHealthTimeout() {
        return healthTimeout;
    }

    public void setHealthTimeout(Long healthTimeout) {
        this.healthTimeout = healthTimeout;
    }

    public boolean isUnrepairable() {
        return unrepairable;
    }

    public void setUnrepairable(boolean unrepairable) {
        this.unrepairable = unrepairable;
    }

    public boolean isUnderRepair() {
        return underRepair;
    }

    public void setUnderRepair(boolean underRepair) {
        this.underRepair = underRepair;
    }

    public List<HealthCommand> getHealthCommands() {
        return healthCommands;
    }

    public void setHealthCommands(List<HealthCommand> healthCommands) {
        this.healthCommands = healthCommands;
    }

    public List<AnalyticData> getAnalyticData() {
        return analyticData;
    }

    public void setAnalyticData(List<AnalyticData> analyticData) {
        this.analyticData = analyticData;
    }


    public List<RepairStep> getRepairSteps() {
        return repairSteps;
    }

    public void setRepairSteps(List<RepairStep> repairSteps) {
        this.repairSteps = repairSteps;
    }

    public void addHealthCommand(HealthCommand command) {
        this.healthCommands.add(command);
    }

    public List<String> getRepairedContainers() {
        return repairedContainers;
    }

    public void setRepairedContainers(List<String> repairedContainers) {
        this.repairedContainers = repairedContainers;
    }

    public void addRepairedContainer(String name) {
        repairedContainers.add(name);
    }

    public void addRepairStep(RepairStep repairStep) {
        repairSteps.add(repairStep);
    }

    public void addAnalyticData(AnalyticData analyticData) {
        this.analyticData.add(analyticData);
    }

    public boolean isCreatingContainer() {
        return creatingContainer;
    }

    public void setCreatingContainer(boolean creatingContainer) {
        this.creatingContainer = creatingContainer;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public boolean isInStep() {
        return isInStep;
    }

    public void setInStep(boolean inStep) {
        isInStep = inStep;
    }

    public void resetRepairStepExecution() {
        for (RepairStep step : repairSteps) {
            step.setExecuted(false);
        }
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {

        }
        return "{}";
    }
}
