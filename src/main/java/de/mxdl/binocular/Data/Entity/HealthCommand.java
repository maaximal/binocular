package de.mxdl.binocular.Data.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Entity that holds the information for a health command that is used by a job
 */
@Entity
@Cacheable(false)
@Table(name = "health_command", indexes = @Index(columnList = "id"))
public class HealthCommand implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @JoinColumn(name = "job")
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    private Job job;
    private CHECKTYPE type;
    @Size(max = 200000)
    private String command;
    private String url;
    private int statusCode;

    public HealthCommand() {
    }

    public HealthCommand(String command, Job job) {
        this.command = command;
        this.type = CHECKTYPE.Command;
        this.job = job;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public CHECKTYPE getType() {
        return type;
    }

    public void setType(CHECKTYPE type) {
        this.type = type;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public enum CHECKTYPE {Command, HttpCall}
}
