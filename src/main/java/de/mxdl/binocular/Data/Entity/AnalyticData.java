package de.mxdl.binocular.Data.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Entity that holds the information about an entry of analytic data about a job
 */
@Entity
@Cacheable(false)
@Table(name = "analytic_data", indexes = @Index(columnList = "id"))
public class AnalyticData implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JoinColumn(name = "job")
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    private Job job;

    private Timestamp timestamp;

    private boolean successful;

    public AnalyticData() {
    }

    public AnalyticData(Job job) {
        this.job = job;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
