binocularModule.controller('analyticsListController', ['$scope', '$http', '$location', '$routeParams',function($scope, $http, $location, $routeParams) {
    $scope.analyticData = [];
    $scope.id = $routeParams.jobid;
    $scope.sortReverse = false;
    $scope.sortBy = 'id';
    $scope.analyticLoadSuccess = 'loading';

    $http({
        method: 'GET',
        url: basePath + '/analytics/' + $scope.id
    }).then(function successCallback(response) {
        $scope.analyticData = response.data.analytics;
        $scope.analyticLoadSuccess = 'success';
    }, function errorCallback(response) {
        console.log("Error loading analytics: " + response.data);
        $scope.analyticLoadSuccess = 'fail';
    });

    $scope.deleteAnalytic = function (id) {
        $scope.deleteSuccess = true;
        $http({
            method: 'DELETE',
            url: basePath + '/analytics/' + $scope.id + '/' + id
        }).then(function successCallback(response) {
            $scope.deleteSuccess = response.status === 200;
            $scope.analyticData = $scope.analyticData.filter(function(item) {return item.id !== id; })

        }, function errorCallback(response) {
            $scope.deleteSuccess = false;
            setTimeout(function(){$scope.apply($scope.deleteSuccess = true)}, 100);
        });
    };

    $scope.redirectAnalyticsId = function (id) {
        $location.path('analytics/' + $scope.id + '/' + id);
    };

    $scope.timestampToDate = function (timestamp) {
        var date = new Date(timestamp);
        return date;

    };



}]);