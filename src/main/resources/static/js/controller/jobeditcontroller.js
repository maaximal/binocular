binocularModule.controller('jobEditController', ['$scope', '$http', '$routeParams', '$location',function($scope, $http, $routeParams, $location) {
    $scope.loading = false;
    $scope.id = $routeParams.jobid;
    $scope.editJobLoadSuccess = 'loading';
    $scope.editJob = "";
    $http({
        method: 'GET',
        url: basePath + '/job/' + $scope.id
    }).then(function successCallback(response) {
        $scope.editJobLoadSuccess = 'success';

        showEditJobForm(response, $scope, $http, $location);
    }, function errorCallback(response) {
        console.log("Error loading job with id: " + $scope.id + " Response: " + response);
        $scope.errorMessage = "Could not load job.";
        if (response.status === 404) $scope.errorMessage = "No job with id: " + $scope.id ;
        $scope.editJobLoadSuccess = 'fail';
    });

}]);

function showEditJobForm(response, $scope, $http, $location) {

    $scope.editJob = response.data;

    $scope.editJobModel = JSON.parse(JSON.stringify($scope.editJob));
    console.log($scope.editJobModel);
    $scope.editJobSchema = {
        "type": "object",
        "properties": {
            "healthInterval": {
                "title": "Interval in which the container's health should be checked in seconds",
                "type": "string",
                "placeholder": "15",
                "pattern": "^[1-9]\\d*$"
            },
            "healthTimeout": {
                "title": "Timeout before a health-check is treated as failed in seconds",
                "type": "string",
                "placeholder": "2",
                "pattern": "^[1-9]\\d*$"
            },
            "healthRetries": {
                "title": "Retries of a failed health-check before a repair is started",
                "type": "string",
                "placeholder": "3",
                "pattern": "^[1-9]\\d*$"
            },
            "healthStartPeriod": {
                "title": "Time in seconds before the first health-check is executed",
                "type": "string",
                "placeholder": "8",
                "pattern": "^[1-9]\\d*$"
            },
            "healthCommands": {
                "title": "Health Checks",
                "type": "array",
                "items": {
                    "title": "Health Check",
                    "type": "object",
                    "properties": {
                        "type": {
                            "title": "Type of check",
                            "type": "string",
                            "enum": [
                                "HttpCall",
                                "Command"
                            ]
                        },

                        "command": {
                            "title": "Command to execute",
                            "type": "string"
                        },
                        "url": {

                            "title": "URL to check",
                            "type": "string"
                        },
                        "statusCode": {
                            "title": "Expected status code",
                            "type": "integer"
                        }
                    }
                }
            },

            "repairSteps": {
                "title": "Steps that should be gone through when trying to repair the container",
                "type": "array",
                "items": {
                    "title": "Repair Step",
                    "type": "object",
                    "properties": {
                        "type": {
                            "title": "Type of repair",
                            "type": "string",
                            "enum": [
                                "Restart",
                                "Replace",
                                "Reset"
                            ]
                        },
                        "replaceOld": {
                            "condition": "type === Reset || type === Replace",
                            "title": "Replace the old container (otherwise it is kept for analytic purposes)",
                            "type": "boolean"
                        }
                    }
                }
            }
        }
    };

    $scope.editJobForm = [
        {
            "type": "help",
            "helpvalue": "<h3 class='center'>Edit job</h3>"
        },
        "healthInterval",
        "healthRetries",
        "healthTimeout",
        "healthStartPeriod",
        {
            "key": "healthCommands",
            "items": [
                "healthCommands[].type",
                {
                    key: "healthCommands[].command",
                    condition: "editJobModel.healthCommands[arrayIndex].type === 'Command'"
                },
                {
                    key: "healthCommands[].url",
                    condition: "editJobModel.healthCommands[arrayIndex].type === 'HttpCall'"
                },
                {
                    key: "healthCommands[].statusCode",
                    condition: "editJobModel.healthCommands[arrayIndex].type === 'HttpCall'"
                }
            ]
        },
        {
            "key": "repairSteps",
            "items": [
                "repairSteps[].type",
                {
                    key: "repairSteps[].replaceOld",
                    condition: "editJobModel.repairSteps[arrayIndex].type === 'Reset' || editJobModel.repairSteps[arrayIndex].type === 'Replace'"
                }
            ]
        }
    ];



    $scope.onSubmitEdit = function(form) {
        $scope.editJobModel = cleanJson($scope.editJobModel);
        $scope.$broadcast('schemaFormValidate');
        console.log("Valid: " + form.$valid);
        if (form.$valid) {
            console.log($scope.editJobModel);
            patchJob($scope.editJobModel, $scope, $http, $location);
        }
    }
}

function cleanJson(body){
    for(entry in body.healthCommands){
       if(body.healthCommands[entry].type === 'HttpCall'){
           delete body.healthCommands[entry].command;
       }
       else{
           delete body.healthCommands[entry].url;
           delete body.healthCommands[entry].statusCode;
       }
    }
    return body;
}

function patchJob(body, $scope, $http, $location) {
    $scope.loading = true;
    $http({
        method: 'PATCH',
        url: basePath + '/job/' + $scope.id ,
        data: body
    })
        .then(function (success) {
            $scope.loading = false;
            $location.path('job/');
        }, function (error) {
            $scope.loading = false;
            $scope.jobEdited = 'fail';
            $scope.editJobError = "";
            if(error.status === 400) $scope.editJobError="Malformed request";
            window.scrollTo(0, 0);
            console.log(error)
        });
}