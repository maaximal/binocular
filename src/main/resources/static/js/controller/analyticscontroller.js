binocularModule.controller('analyticsController', ['$scope', '$http', '$location', '$routeParams',function($scope, $http, $location, $routeParams) {
    $scope.urls = [];
    $scope.id = $routeParams.jobid;
    $scope.analyticId = $routeParams.analyticid;
    $scope.analyticIdLoadSuccess = 'loading';

    $http({
        method: 'GET',
        url: basePath + '/analytics/' + $scope.id + "/" + $scope.analyticId
    }).then(function successCallback(response) {
        $scope.urls = response.data.files;
        $scope.analyticIdLoadSuccess = 'success';
    }, function errorCallback(response) {
        console.log("Error loading analytics: " + response.data);
        $scope.analyticIdLoadSuccess = 'fail';
    });

    $scope.redirectAnalyticsFile = function (file) {
        window.location.href = window.location.origin + '/data' + file;
    };



}]);