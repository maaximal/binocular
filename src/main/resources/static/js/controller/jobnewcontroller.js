binocularModule.controller('newJobController', ['$scope', '$http', '$location', function ($scope, $http, $location) {
    $scope.loading = false;
    $scope.containerLoadSuccess = 'loading';
    $scope.loading = false;
    $scope.jobCreated = '';
    $http({
        method: 'GET',
        url: basePath + '/container'
    }).then(function successCallback(response) {
        containerNameFromJson(response, $scope, $http, $location);
        $scope.containerLoadSuccess = 'success';
    }, function errorCallback(response) {
        console.log("Error loading jobs " + response);
        $scope.containerLoadSuccess = 'fail';
    });
}]);

function containerNameFromJson(response, $scope, $http, $location) {
    $scope.containers = [];

    var containerList = response.data.containers;
    for (var container in containerList) {
        var name = containerList[container].Names[0].replace("/", "");
        $scope.containers.push(name);
    }
    showNewJobForm($scope, $http, $location);
}

function showNewJobForm($scope, $http, $location) {
    $scope.jobSchema = {
        "type": "object",
        "properties": {
            "containerName": {
                "title": "* Container to monitor",
                "type": "string",
                "enum": $scope.containers
            },
            "healthInterval": {
                "title": "Interval in which the container's health should be checked in seconds",
                "type": "string",
                "placeholder": "15",
                "pattern": "^[1-9]\\d*$"
            },
            "healthTimeout": {
                "title": "Timeout before a health-check is treated as failed in seconds",
                "type": "string",
                "placeholder": "2",
                "pattern": "^[1-9]\\d*$"
            },
            "healthRetries": {
                "title": "Retries of a failed health-check before a repair is started",
                "type": "string",
                "placeholder": "3",
                "pattern": "^[1-9]\\d*$"
            },
            "healthStartPeriod": {
                "title": "Time in seconds before the first health-check is executed",
                "type": "string",
                "placeholder": "8",
                "pattern": "^[1-9]\\d*$"
            },

            "healthCommands": {
                "title": "Health Checks",
                "type": "array",
                "items": {
                    "title": "Health Check",
                    "type": "object",
                    "properties": {
                        "type": {
                            "title": "Type of check",
                            "type": "string",
                            "enum": [
                                "HttpCall",
                                "Command"
                            ]
                        },

                        "command": {
                            "title": "Command to execute",
                            "type": "string"
                        },
                        "url": {
                            "title": "URL to check",
                            "type": "string"
                        },
                        "statusCode": {
                            "title": "Expected status code",
                            "type": "integer"
                        }
                    }
                }
            },

            "repairSteps": {
                "title": "Steps that should be gone through when trying to repair the container",
                "type": "array",
                "items": {
                    "title": "Repair Step",
                    "type": "object",
                    "properties": {
                        "type": {
                            "title": "Type of repair",
                            "type": "string",
                            "enum": [
                                "Restart",
                                "Replace",
                                "Reset"
                            ]
                        },
                        "replaceOld": {
                            "title": "Replace the old container (otherwise it is kept for analytic purposes)",
                            "type": "boolean"
                        }
                    }
                }
            }
        },
        "required": [
            "containerName"
        ]
    };

    $scope.jobForm = [
        {
            "type": "help",
            "helpvalue": "<h3 class='center'>Edit job</h3>"
        },
        {
            "type": "help",
            "helpvalue": "<p>Fields marked with * are required</p>"
        },
        "containerName",
        "healthInterval",
        "healthRetries",
        "healthTimeout",
        "healthStartPeriod",
        {
            "key": "healthCommands",
            "items": [
                "healthCommands[].type",
                {
                    key: "healthCommands[].command",
                    condition: "jobModel.healthCommands[arrayIndex].type === 'Command'"
                },
                {
                    key: "healthCommands[].url",
                    condition: "jobModel.healthCommands[arrayIndex].type === 'HttpCall'"
                },
                {
                    key: "healthCommands[].statusCode",
                    condition: "jobModel.healthCommands[arrayIndex].type === 'HttpCall'"
                }
            ]
        },

        {
            "key": "repairSteps",
            "items": [
                "repairSteps[].type",
                {
                    key: "repairSteps[].replaceOld",
                    condition: "jobModel.repairSteps[arrayIndex].type === 'Reset' || jobModel.repairSteps[arrayIndex].type === 'Replace'"
                }
            ]
        }
    ];

    $scope.jobModel = {"containerName": $scope.containers[0]};

    $scope.onSubmit = function (form) {
        $scope.jobModel = cleanJson($scope.jobModel);
        $scope.$broadcast('schemaFormValidate');
        console.log("Valid: " + form.$valid);
        if (form.$valid) {
            console.log($scope.jobModel);
            createJob($scope.jobModel, $scope, $http, $location);
        }
    }
}

function createJob(body, $scope, $http, $location) {
    $scope.loading = true;
    $http({
        method: 'POST',
        url: basePath + '/job',
        data: body
    })
        .then(function (success) {
            $scope.loading = false;
            $location.path('job/');
        }, function (error) {
            $scope.loading = false;
            $scope.jobCreated = 'fail';
            $scope.jobCreatedError = "";
            if (error.status === 409) $scope.jobCreatedError = 'Job for container already exists';
            window.scrollTo(0, 0);
            console.log(error)
        });
}