binocularModule.controller('jobController', ['$scope', '$http', '$location',function($scope, $http, $location) {
    $scope.jobs = [];
    $scope.sortReverse = false;
    $scope.sortBy = 'id';
    $scope.jobLoadSuccess = 'loading';


    $http({
        method: 'GET',
        url: basePath + '/job'
    }).then(function successCallback(response) {
        $scope.jobs = response.data.jobs;
        $scope.jobLoadSuccess = 'success';
    }, function errorCallback(response) {
        console.log("Error loading jobs: " + response.data);
        $scope.jobLoadSuccess = 'fail';
    });

    $scope.deleteJob = function (id) {
        $scope.deleteSuccess = true;
        $http({
            method: 'DELETE',
            url: basePath + '/job/' + id
        }).then(function successCallback(response) {
            $scope.deleteSuccess = response.status === 200;
            $scope.jobs = $scope.jobs.filter(function(item) {return item.id !== id; })

        }, function errorCallback(response) {
            $scope.deleteSuccess = false;
            setTimeout(function(){$scope.apply($scope.deleteSuccess = true)}, 100);
        });
    };
    $scope.redirectJob = function (id) {
        $location.path('job/' + id);
    };
    $scope.redirectAnalytics = function (id) {
        $location.path('analytics/' + id);
    }



}]);