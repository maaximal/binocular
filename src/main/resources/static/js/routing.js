binocularModule.config(function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl : 'template/job.html',
            controller  : 'jobController'
        })
        .when('/job', {
            templateUrl : 'template/job.html',
            controller  : 'jobController'
        })
        .when('/job/new', {
        templateUrl : 'template/newjob.html',
        controller  : 'newJobController'
    })
        .when('/job/:jobid', {
            templateUrl: 'template/editjob.html',
            controller: 'jobEditController'
        })
        .when('/analytics/:jobid', {
            templateUrl : 'template/analytics.html',
            controller  : 'analyticsListController'
        })
        .when('/analytics/:jobid/:analyticid', {
            templateUrl : 'template/analyticid.html',
            controller  : 'analyticsController'
        });
});
